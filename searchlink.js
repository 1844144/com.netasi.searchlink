cj(function(){

function url2arr(search){
	return search?JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g,'":"') + '"}',
                 function(key, value) { return key===""?value:decodeURIComponent(value) }):{}

}




function form_submit(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}

var form_elem = cj('form[class*=Form_Search]');
result_arr = form_elem.find('input[type=submit][value=Search]:last');
search_button =  result_arr.parent();
if (cj('#searchlink').length == 0) {
	clone = search_button.clone();
	clone.css('margin-left', '1cm');
	clone.prop('id','searchlink');
	button = clone.find('input');
	button.prop('type','button');
	button.prop('value', 'Get search link')
	clone.insertAfter(search_button);
	button.click(function(){
		// cj.post(form_elem.attr('action'),form_elem.serialize());
		window.prompt('Here is your link:',
			'http://'+window.location.hostname+'/wp-admin/admin.php?page=CiviCRM&q=civicrm/searchlink/doit/&action='+encodeURIComponent(form_elem.attr('action'))+'&ppparam='+encodeURIComponent(form_elem.serialize()));
	});
} 
// cloning div , making button

// now , what if we have ppparam ?
if (('ppparam-active' in localStorage ) && (localStorage['ppparam-active']=='true')){
	// decode params
	given = url2arr(localStorage['ppparam']);
	current = url2arr(form_elem.serialize());
	given.qfKey = current.qfKey;
	// fix urlencoded keys
	for (k in given) {
		if (k.indexOf('%')!=-1) {
			new_k = decodeURIComponent(k);
			given[new_k] = given[k];
		}
	} 
	localStorage['ppparam-active'] = 'false';
	form_submit(form_elem.attr('action'), given);

}




})
