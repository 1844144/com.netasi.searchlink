<?php

require_once 'CRM/Core/Page.php';

class CRM_Searchlink_Page_Doit extends CRM_Core_Page {
  function run() {
    // Example: Set the page-title dynamically; alternatively, declare a static title in xml/Menu/*.xml
    // CRM_Utils_System::setTitle(ts('Doit'));

    // // Example: Assign a variable for use in a template
    // $this->assign('currentTime', date('Y-m-d H:i:s'));
    CRM_Core_Resources::singleton()->addScriptFile('com.netasi.searchlink', 'doit.js');

    parent::run();
  }
}
