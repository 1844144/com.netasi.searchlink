function url2arr(search){
	return search?JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g,'":"') + '"}',
                 function(key, value) { return key===""?value:decodeURIComponent(value) }):{}

}

cj(function(){
	arr = url2arr(window.location.search.substring(1));
	localStorage['ppparam'] = arr.ppparam;
	// activate it
	localStorage['ppparam-active'] = 'true';
	// go
	window.location = arr.action;
});

